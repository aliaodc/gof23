package com.zwt.visitor;

import com.zwt.shapes.Circle;
import com.zwt.shapes.CompoundShape;
import com.zwt.shapes.Dot;
import com.zwt.shapes.Rectangle;

public interface Visitor {
    String visitDot(Dot dot);

    String visitCircle(Circle circle);

    String visitRectangle(Rectangle rectangle);

    String visitCompoundGraphic(CompoundShape cg);
}