package com.zwt.strategies;
/**
 * @author ML李嘉图
 * @version createtime: 2022-01-14
 * Blog: https://www.cnblogs.com/zwtblog/
 */
/**
 * Common interface for all strategies.
 */
public interface PayStrategy {
    boolean pay(int paymentAmount);

    void collectPaymentDetails();
}