package com.zwt.decorators;

import java.io.File;
import java.io.IOException;
/*
原始值
写时：
先压缩
再加密

读时：
先解密
再解压
恢复原始值
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        String fileName = "d:/out/OutputDemo.txt";
        File file = new File(fileName);
        if (!file.exists()){
            File parentFile = file.getParentFile();
            if (!parentFile.exists()){
                parentFile.mkdirs();
            }
            file.createNewFile();
            System.out.println("file not exists");
        }else {
            System.out.println("file  exists");
        }
        String salaryRecords = "Name,Salary\nJohn Smith,100000\nSteven Jobs,912000";
        DataSourceDecorator encoded = new CompressionDecorator(
                new EncryptionDecorator(
                        new FileDataSource(fileName)));
        encoded.writeData(salaryRecords);
        DataSource plain = new FileDataSource(fileName);

        System.out.println("- Input ----------------");
        System.out.println(salaryRecords);
        System.out.println("- Encoded --------------");
        System.out.println(plain.readData());
        System.out.println("- Decoded --------------");
        System.out.println(encoded.readData());
    }
}
