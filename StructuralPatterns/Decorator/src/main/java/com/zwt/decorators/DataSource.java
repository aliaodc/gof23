package com.zwt.decorators;

public interface DataSource {
    void writeData(String data);

    String readData();
}
