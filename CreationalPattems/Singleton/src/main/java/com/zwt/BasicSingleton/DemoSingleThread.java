package com.zwt.BasicSingleton;
/**
 * @author ML李嘉图
 * @version createtime: 2021-11-09
 * Blog: https://www.cnblogs.com/zwtblog/
 */
public class DemoSingleThread {
    public static void main(String[] args) {
        System.out.println("If you see the same value, then singleton was reused (yay!)" + "\n" +
                "If you see different values, then 2 singletons were created (booo!!)" + "\n\n" +
                "RESULT:" + "\n");

        Singleton anotherSingleton = Singleton.getInstance("wb");
        Singleton singleton = Singleton.getInstance("zwt");

        System.out.println(singleton.value);
        System.out.println(anotherSingleton.value);
    }
}
