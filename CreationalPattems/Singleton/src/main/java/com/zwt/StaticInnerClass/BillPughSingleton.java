package com.zwt.StaticInnerClass;

public class BillPughSingleton {
    public String value;

    private BillPughSingleton(String value){
        this.value = value;
    }
    
    private static class SingletonHelper{
        private static final BillPughSingleton INSTANCE = new BillPughSingleton("init");
    }
    
    public static BillPughSingleton getInstance(){
        return SingletonHelper.INSTANCE;
    }
}

