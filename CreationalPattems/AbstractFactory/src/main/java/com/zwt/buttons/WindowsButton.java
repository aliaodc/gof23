package com.zwt.buttons;

/**
 * @author ML李嘉图
 * @version createtime: 2021-11-10
 * Blog: https://www.cnblogs.com/zwtblog/
 */

/**
 * All products families have the same varieties (MacOS/Windows).
 * <p>
 * This is another variant of a button.
 */
public class WindowsButton implements Button {

    @Override
    public void paint() {
        System.out.println("You have created WindowsButton.");
    }
}